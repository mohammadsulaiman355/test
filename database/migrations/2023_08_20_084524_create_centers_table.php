<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('centers', function (Blueprint $table) {
            $table->id();
			$table->timestamps();
			$table->softDeletes();
			$table->string('name', 80);
			$table->string('address', 200);
			$table->string('notes', 200)->nullable();
			$table->string('center-image')->default('no-center-image.png');
			$table->foreignId('city_id')->constrained()->onDelete('cascade');
            $table->integer('center_id')->unsigned()->nullable();
            $table->foreign('center_id')->references('id')->on('centers')->onDelete('cascade');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
        });
    }
    public function down(): void
    {
        Schema::dropIfExists('centers');
    }
};
