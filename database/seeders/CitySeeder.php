<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // City::create([]);
        DB::table('cities')->insert([
            'name'=>'قامشلو',
            'code'=>'1100',
            'country_id'=>1
        ]);
        DB::table('cities')->insert([
            'name'=>'دير الزور',
            'code' =>'22',
            'country_id'=>2
        ]);
    }
}
