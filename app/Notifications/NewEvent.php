<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class NewEvent extends Notification
{
    use Queueable;
    private $center;
    public function __construct($center)
    {
        $this->center=$center;
    }
    public function via(object $notifiable): array
    {
        return ['database'];
    }
    public function toArray(object $notifiable): array
    {
        return [
            'profile' => $this->center->user->profile,
            'username' => $this->center->user->name,
            'name' => $this->center->name,
        ];
    }
}
