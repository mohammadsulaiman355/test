<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CenterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    public function rules(): array
    {
        return [
            'name'=>[
                'required',
                
            ],
            'address'=>[
                'required',
            ],
            'city_id'=>[
                'required'
            ],
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'الرجاء ملئ الحقل',
            // 'name.max' => 'الحد الأقصى هي 80 حرف ',
            // 'email.required' => 'The email field is required.',
            // 'email.email' => 'Please enter a valid email address.',
            // 'email.unique' => 'This email address is already registered.',
            // Other custom messages
        ];
    }
}
