<?php

namespace App\Http\Controllers;


use App\Http\Requests\CenterRequest;
use App\Models\Center;
use App\Models\City;
use App\Models\User;
use App\Notifications\deleteCenter;
use App\Notifications\NewEvent;
use App\Notifications\Restore;
use App\Notifications\UpdateCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Helper\CustomHelper;
/**
 * Custom Helper Class
 *
 * @author Mohammad Sulaiman
 */

class CenterController extends Controller
{
    // field input 

    private $fields = ['name', 'notes', 'city_id', 'parent_id', 'user_id', 'address'];

    // this property for model object 

    private $model;

    // name of blade files 

    private $fileBlade = 'centers';

    public function construct()
    {

        $this->model = new Center(); // initialize the property 
    }
    /**
     * Display the index page with a list of Currency records.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */

    public static function message($route, $type, $content)
    {
        return redirect()->route($route)->with($type, $content);
    }
    public function index()
    {
        try {
            $centers = Center::paginate(10);
            return view('centers.index', compact('centers'));
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    public function create()
    {
        try {
            $cities = City::get();
            $centers = Center::get();
            // Return the create view with the user's notificationsc
            return view($this->fileBlade . '.create', compact('cities', 'centers'));
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    public function store(CenterRequest $request)
    {
        try {
            // Prepare data for creating a new Currency record

            $data = CustomHelper::prepareData($request, $this->fields);

            // $center = Center::create($request->all() + ['user_id' => Auth::user()->id]);
            $center=$this->model::create($data);
            if ($center->save()) {
                $users = User::all();
                Notification::send($users, new NewEvent($center));
                return self::message('centers.index', 'message', 'creating center successfuly');
            } else
                return back();
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    public function show(Center $center)
    {
        return view('centers.show', compact('center'));
    }
    public function edit(Center $center)
    {
        try {
            $cities = City::all();
            $centers = Center::all();
            return view('centers.edit', compact('center', 'cities', 'centers'));
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    public function update(Request $request, $id) // why it's not working with CenterRequest.
    {
        try {
            $item = Center::findOrFail($id);
            $old = $item->name;
            $newData = $request->all();
            if ($this->dataHasChanged($item, $newData)) {
                if ($item->update($request->all())) {
                    $users = User::all();
                    Notification::send($users, new UpdateCenter($item, $old));
                    return redirect()->route('centers.index')->with('message', 'تم تعديل المركز بنجاح');
                }
            } else return back()->with('error', 'حدث مشكلة ما');
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    public function destroy(Center $center)
    {
        try {
            $center1 = $center;
            if ($center->delete()) {
                $users = User::all();
                $user = User::findOrFail(Auth::user()->id);
                Notification::send($users, new deleteCenter($center1, $user));
                return redirect()->route('centers.index')->with('message', 'تم حذف المركز بنجاح');
            } else
                return back();
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما');
        }
    }
    public function archive()
    {
        try {
            $centers = Center::onlyTrashed()->paginate(10);
            return view('centers.archive', compact('centers'));
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    public function trash($id)
    {
        try {
            $center = Center::onlyTrashed()->findOrFail($id);
            $center->forceDelete();
            return redirect()->route('centers.index')->with('message', 'تم حذف المركز بشكل نهائي');
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    public function restore(Center $center)
    {
        try {
            if ($center->restore()) {
                $users = User::get();
                Notification::send($users, new Restore($center));
                return redirect()->route('centers.index');
            } else
                return back();
        } catch (\Exception $e) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما');
        }
    }
    public function ajax_search(Request $request)
    {
        try {
            if ($request->ajax()) {
                $search_by_text = $request->search_by_text;
                if (!empty($search_by_text)) {
                    $centers = Center::where('name', 'LIKE', '%' . $search_by_text . '%')->get();
                    return view('centers.ajax_search', compact('centers'));
                } else {
                    $data = Center::select()->orderby('id', 'ASC');
                    return view('centers.ajax_search', compact('centers'));
                }
            }
        } catch (\Exception $ex) {
            return self::message('centers.index', 'error', 'هنالك خطأ ما ');
        }
    }
    // function for check if data are changed or not 
    // it's for update action
    private function dataHasChanged($item, $newData)
    {
        foreach ($newData as $key => $value) {
            if ($item->$key != $value) {
                return true;
            }
        }
        return false;
    }
}
