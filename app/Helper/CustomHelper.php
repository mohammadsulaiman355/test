<?php
namespace App\Helper;

/**
 * Custom Helper Class
 *
 * @author Mohammad Sulaiman
 */

class CustomHelper
{
    public static function getPaginatedModelRecords($model)
    {
        return $model->select()->orderby('id', 'ASC')->paginate(PAGINATION_COUNT);
    }
    /**
     * Prepare the data for creating a record.
     *
     * @param \Illuminate\Http\Request $request The request object.
     * @param array $fields The fields to prepare.
     * @return array The prepared data.
     */
    
    public static function prepareData($request, $fields)
    {
        $requestData = [];

        foreach ($fields as $attribute) {
            // If the attribute is 'parent_id' and it's null, include it in the array
            if ($attribute == 'parent_id' && $request->$attribute == null) {
                $requestData[$attribute] = null;
            } elseif ($attribute == 'password') {
                // If the attribute is 'password', hash it before adding to the array
                $requestData[$attribute] = bcrypt($request->$attribute);
            } else {
                $requestData[$attribute] = $request->$attribute;
            }
        }

        return $requestData;
    }
    public static function s(){
        return "Hello";
    }
}
