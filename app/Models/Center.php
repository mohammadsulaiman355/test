<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Center extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded=[];
    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }
    public function parent(){
        return $this->belongsTo(Self::class,'center_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
