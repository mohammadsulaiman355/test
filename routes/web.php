<?php

use App\Http\Controllers\CenterController;
use App\Http\Controllers\CityController;
use App\Models\Center;
use App\Models\City;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
| 
*/
define('PAGINATION_COUNT',10);


Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', function () {
    return view('dashboardpanal.admin.welcome');
})->name('dashboard');
Route::middleware('auth')->group(function(){
    Route::resource('centers',CenterController::class);
    Route::resource('cities',CityController::class);
    Route::get('centers-archive',[CenterController::class,'archive'])->name('centers-archive');
    Route::get('trash/{id}',[CenterController::class,'trash'])->withTrashed()->name('trash');
    Route::get('restore/{center}',[CenterController::class,'restore'])->withTrashed()->name('restore');
    Route::post('/currency/ajax_search', [CenterController::class,'ajax_search'])->name('currency.ajax_search');
    Route::get('noti',function (){
        $user=Auth::user();
        foreach($user->unreadNotifications as $notification){
            $notification -> markAsRead();
        }
        // return $notifications;
        return view('notifications.index',compact('user'));
    })->name('noti');
    Route::get('read11/{id}',function($id){
        $notification = auth()->user()->notifications()->where('id', $id)->first();
            if ($notification) {
                $notification->markAsRead();
                return back();
            }
    })->name('read11');
    Route::get('test',function (){
        $center=Center::findOrFail(1);
        return $center->user->name;
    });
    Route::get('profile',function (){
        $user=User::findOrFail(Auth::user()->id);
        return view('profile',compact('user'));
    });
});
Auth::routes();