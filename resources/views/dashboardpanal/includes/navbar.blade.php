<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
     </li>
     <li class="nav-item d-none d-sm-inline-block">
       @yield('breadcrumb')
     </li>
  </ul>
  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto " style="float: right; margin-left: 20px!important">
     <li class="nav-item dropdown">
        <a href="{{route('noti')}}" class="nav-link">
        <i class="far fa-bell"></i>
        <span class="badge badge-warning navbar-badge"><span id="count-noti">{{auth()->user()->unreadNotifications->count()}}</span></span>
        </a>
        {{-- <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
           <span class="dropdown-header">15 Notifications</span>
           <div class="dropdown-divider"></div>
           <a href="#" class="dropdown-item">
           <i class="fas fa-envelope mr-2"></i> 4 new messages
           <span class="float-right text-muted text-sm">3 mins</span>
           </a>
           <div class="dropdown-divider"></div>
           <a href="#" class="dropdown-item">
           <i class="fas fa-users mr-2"></i> 8 friend requests
           <span class="float-right text-muted text-sm">12 hours</span>
           </a>
           <div class="dropdown-divider"></div>
           <a href="#" class="dropdown-item">
           <i class="fas fa-file mr-2"></i> 3 new reports
           <span class="float-right text-muted text-sm">2 days</span>
           </a>
           <div class="dropdown-divider"></div>
           <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div> --}}
     </li>
     <li class="nav-item">
        <a class="justify-content-center align-items-center ">
          <form action="{{ route('logout') }}" method="POST">
            @csrf
            <button href="#" class="btn btn-outline btn-lm" type="submit" style="background-color: #81ecec">
               <span class="glyphicon glyphicon-log-out"></span> <i class="fa-solid fa-arrow-right-from-bracket"></i>
            </button>
       </form>
         </a>
     </li>
  </ul>
</nav>