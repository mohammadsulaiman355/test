<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="" class="brand-link">
  <img src="{{ asset('assets/admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
     style="opacity: .8">
  <span class="brand-text font-weight-light">AcornAssociated</span>
  </a>
  <div class="sidebar">
     <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
           <img src="{{asset('storage/profiles/'.auth()->user()->profile)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
           <a href="#" class="d-block">@auth {{ auth()->user()->name }} @endauth</a>
        </div>
     </div>
     <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
           <li class="nav-item has-treeview  {{ (request()->is('centers') || request()->is('centers-archive')||request()->is('centers/*') )?'menu-open':'' }}     ">
            <a href="#" class="nav-link {{ (request()->is('centers')||request()->is('centers/*') )?'active':'' }}">
               <p>
                  <i class="fa-regular fa-building"></i>
                  المراكز
                  <i class="right fas fa-angle-left"></i>
               </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                  <a href="{{route('centers.index')}}" class="nav-link {{ (request()->is('centers') )?'active':'' }}">
                     <p>جميع المراكز</p>
                  </a>
               </li>
            </ul>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                  <a href="{{route('centers-archive')}}" class="nav-link {{ (request()->is('centers-archive') )?'active':'' }}">
                     <i class="fa-solid fa-trash"></i>
                     <p>سلة المحذوفات</p>
                  </a>
               </li>
            </ul>
         </li>
        </ul>
     </nav>
  </div>
</aside>
