@extends('dashboardpanal.layouts.admin')
<link rel="stylesheet" href="{{asset('notifications.css')}}">
@section('title')
@endsection

@section('breadcrumb')
<a href="{{ route('centers.index') }}" class="nav-link">المؤسسات</a>
@endsection

@section('contentheader')

@endsection

@section('contentheaderlink')
عرض جميع المؤسسات
@endsection

@section('contentheaderactive')
المؤسسات
@endsection
@section('content')
<section class="section-50">
    <div class="container">
        <h3 class="m-b-50 heading-line">الإشعارات <i class="fa fa-bell text-muted"></i></h3>
            
    @forelse ($user->notifications as $notification)
        @if ($notification->type == 'App\Notifications\NewEvent')
        <div class="notification-ui_dd-content">
            <div class="notification-list notification-list--unread"  style="background-color: #81ecec">
                <div class="notification-list_content ">
                    <div class="notification-list_img">
                        <img src="{{asset('storage/profiles/'.$notification->data['profile'])}}" alt="user">
                    </div>
                    <div class="notification-list_detail">
                        <span class="font-weight-bolder">{{$notification->data['username']}}</span>
                        <span> انشأ مؤسسة جديدة (<b>{{$notification->data['name']}}</b>)</span>
                        {{-- <p class="text-muted"><small>10 mins ago</small></p> --}}
                    </div>
                </div>
                <div class="notification-list_feature-img">
                    {{-- <img src="https://i.imgur.com/AbZqFnR.jpg" alt="Feature image"> --}}
                </div>
            </div>
            @endif
            @if ($notification->type == 'App\Notifications\UpdateCenter')
            <div class="notification-ui_dd-content text-decoration-none text-black" ">
                {{-- todo href="{{route('centers.index')}} --}}
                <div class="notification-list notification-list--unread" style="background-color:#a29bfe">
                    <div class="notification-list_content">
                        <div class="notification-list_img">
                            <img src="{{asset('storage/profiles/'.$notification->data['profile'])}}" alt="user">
                        </div>
                        <div class="notification-list_detail">
                            <span class="font-weight-bolder">{{$notification->data['username']}}</span>
                            <span> تم تعديل بيانات المركز من (<b>{{$notification->data['old']}}</b>)  الى (<b>{{$notification->data['name']}}</b>)</span>
                            {{-- <p class="text-muted"><small>10 mins ago</small></p> --}}
                        </div>
                    </div>
                    <div class="notification-list_feature-img">
                        {{-- <img src="https://i.imgur.com/AbZqFnR.jpg" alt="Feature image"> --}}
                    </div>
                </div>
                @endif
                @if ($notification->type == 'App\Notifications\deleteCenter')
                <div class="notification-ui_dd-content text-decoration-none text-black" >
                    {{-- todo href="{{route('centers.index')}} --}}
                    <div class="notification-list notification-list--unread" style="background-color: #fab1a0">
                        <div class="notification-list_content">
                            <div class="notification-list_img">
                                <img src="{{asset('storage/profiles/'.$notification->data['profile'])}}" alt="user">
                            </div>
                            <div class="notification-list_detail">
                                <span class="font-weight-bolder">{{$notification->data['username']}}</span>
                                <span> تم نقل المؤسسة (<b>{{$notification->data['name']}}</b>) الى سلة المحذوفات </span>
                            </div>
                        </div>
                        <div class="notification-list_feature-img">
                            {{-- <img src="https://i.imgur.com/AbZqFnR.jpg" alt="Feature image"> --}}
                        </div>
                    </div>
                    @endif
                    @if ($notification->type == 'App\Notifications\Restore')
        <div class="notification-ui_dd-content">
            <div class="notification-list notification-list--unread"  style="background-color: #81ecec">
                <div class="notification-list_content ">
                    <div class="notification-list_img">
                        <img src="{{asset('storage/profiles/'.$notification->data['profile'])}}" alt="user">
                    </div>
                    <div class="notification-list_detail">
                        <span class="font-weight-bolder">{{$notification->data['username']}}</span>
                        <span> تم استعادة المؤسسة (<b>{{$notification->data['name']}}</b>)</span>
                        {{-- <p class="text-muted"><small>10 mins ago</small></p> --}}
                    </div>
                </div>
                <div class="notification-list_feature-img">
                    {{-- <img src="https://i.imgur.com/AbZqFnR.jpg" alt="Feature image"> --}}
                </div>
            </div>
            @endif
    </div>
    @empty
    <div class="text-center text-info">لا توجد إشعارات</div>
    @endforelse
    <div class="text-center">
        <a href="#!" class="dark-link">Load more activity</a>
    </div>
</section>
@endsection