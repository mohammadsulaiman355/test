@extends('dashboardpanal.layouts.admin')

@section('title')
@endsection

@section('breadcrumb')
<a href="{{ route('centers.index') }}" class="nav-link">المؤسسات</a>
@endsection

@section('contentheader')

@endsection
@section('contentheaderlink')
عرض جميع المؤسسات
@endsection

@section('contentheaderactive')
المؤسسات
@endsection
@section('content')
<input type="hidden" id="token_search" value="{{csrf_token()}}">
<input type="hidden" id="ajax_search_url" value="{{ route('currency.ajax_search') }}">
<a href="{{route('centers.create')}}" class="btn btn-outline-success ml-auto mr-auto d-block mb-3 w-25">انشاء مؤسسة جديدة</a>
<div class="input-group rounded w-50 mr-auto ml-auto mb-3">
  <input type="search" class="form-control rounded" placeholder="بحث" id="search_by_text" aria-label="Search" aria-describedby="search-addon" />
  </span>
</div>
  <table class="table">
    <thead style="background-color:#81ecec ">
      <tr>
        <th scope="col">#</th>
        <th scope="col">اسم المركز</th>
        <th scope="col">المدينة</th>
        <th scope="col">العنوان</th>
        <th scope="col">تابعة ل</th>
        <th scope="col">ملاحظات</th>
        <th scope="col">افعال</th>
      </tr>
    </thead>
    <tbody id="itemTableBody">
      <?php $i=0?>
      @foreach ($centers as $center)
      <tr>
        <th scope="row">{{++$i}}</th>
        <td>{{Str::limit($center->name,30)}}</td>
        <td>{{$center->city->name }}</td>
        <td>{{Str::limit($center->address,30)}}</td>
        @if ($center->center_id != null)
        @if (isset($center->parent->name))
        <td>{{Str::limit($center->parent->name,30)}}</td>
        @else
        <td>-</td>
        @endif
        @else
        <td>-</td>
        @endif
        <td>{{Str::limit($center->notes,20)}}</td>
        <td>
          <a href="{{route('centers.show',$center)}}" class="btn btn-outline-info"><i class="fa-solid fa-eye"></i></a>
          <a href="{{route('centers.edit',$center)}}" class="btn btn-outline-warning"><i class="fa-solid fa-pen-to-square"></i></a>
          <form action="{{route('centers.destroy',$center)}}" class="d-inline-block" method="POST">
            @csrf
            @method('DELETE')
            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal"> <i class="fa-solid fa-trash"></i></button>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-confirm">
                  <div class="modal-content">
                      <div class="modal-header">
                          <div class="icon-box">
                              <i class="bi bi-x-lg" style="font-size: 2rem; color: #f15e5e;"></i>
                          </div>
                      </div>
                      <h4 class="modal-title">تأكيد عملية الحذف؟</h4>
                      <div class="modal-body">
                        <p>هل تريد حذف المؤسسة المحددة بشكل نهائي.</p>
                       </div>
                      <div class="modal-footer" style="margin-left:40px;">
                          <button type="submit" class="btn btn-danger">حذف</button>
                          <a type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</a>
                      </div>
                  </div>
              </div>
          </div>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="d-flex">
    {!! $centers->links() !!}
  </div>
@endsection