@foreach ($centers as $center)
<tr>
  <th scope="row">{{$center->id}}</th>
  <td>{{$center->name}}</td>
  <td>{{$center->city->name}}</td>
  <td>{{$center->address}}</td>
  @if ($center->center_id != null)
  <td>{{$center->parent->name}}</td>
  @else
  <td>-</td>
  @endif
  <td>{{$center->notes}}</td>
  <td>
    <a href="{{route('centers.show',$center)}}" class="btn btn-outline-info"><i class="fa-solid fa-eye"></i></a>
    <a href="{{route('centers.edit',$center)}}" class="btn btn-outline-warning"><i class="fa-solid fa-pen-to-square"></i></a>
    <form action="{{route('centers.destroy',$center)}}" class="d-inline-block" method="POST">
      @csrf
      @method('DELETE')
      <button class="btn btn-outline-danger" type="submit"><i class="fa-solid fa-trash"></i></button>
    </form>
  </td>
</tr>
@endforeach