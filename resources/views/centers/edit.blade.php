@extends('dashboardpanal.layouts.admin')

@section('title')
@endsection

@section('breadcrumb')
    <a href="{{ route('dashboard') }}" class="nav-link"> تعديل</a>
@endsection

@section('contentheader')
@endsection

@section('contentheaderlink')
تعديل المؤسسة
@endsection

@section('contentheaderactive')
المؤسسات
@endsection

@section('content')
    <form action="{{ route('centers.update', $center) }}" method="POST">
        @csrf
        @method('PUT')
        <!-- 2 column grid layout with text inputs for the first and last names -->
        <div class="row mb-4">
        </div>
        <!-- Text input -->
        <div class="form-outline mb-4">
            <input type="text" id="form6Example3" class="form-control" name="name" value="{{ $center->name }}"/>
            <label class="form-label text-primary" for="form6Example3">اسم المؤسسة</label>
        </div>
        <!-- Text input -->
        <div class="form-outline mb-4">
            <input type="text" id="form6Example4" class="form-control" name="address" value="{{ $center->address }}" />
            <label class="form-label text-primary" for="form6Example4">العنوان</label>
        </div>
        <!-- Message input -->
        <div class="form-outline mb-4">
            <textarea class="form-control" id="form6Example7" rows="4" name="notes">{{ $center->notes }}</textarea>
            <label class="form-label" for="form6Example7">ملاحظات</label>
        </div>
        {{-- <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
            @foreach ($cities as $city)
                <option value="{{$city->id}}" {{ $city->id == $center->city_id ? 'selected' : '' }}>
                    {{$city->name}}
                </option>
            @endforeach
        </select> --}}
        <div class="col-md-3 mb-3">
 
            <label for="validationDefault04"> اختر مدينة</label>
            <select class="custom-select" id="validationDefault04" required name="city_id">
                @foreach ($cities as $city)
                <option value="{{$city->id}}" {{ $city->id == $center->city_id ? 'selected' : '' }}>
                    {{$city->name}}
                </option>
            @endforeach
            </select>
          </div>
          @error('city_id')
          <div class="text-danger">{{$message}}</div>
          @enderror
        {{-- <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="center_id">
            @foreach ($centers as $center)
                @if ($center->id != $center->id)
                    <option value="{{ $center->id }}">{{ $center->name }}</option>
                @else
                @endif
            @endforeach
        </select> --}}
        <div class="col-md-3 mb-3">
            <label for="validationDefault04">تابعة ل</label>
            <select class="custom-select" id="validationDefault04" name="center_id">
                @foreach ($centers as $center)
                @if ($center->id != $center->id)
                    <option value="{{ $center->id }}">{{ $center->name }}</option>
                @else
                @endif
            @endforeach
            </select>
          </div>
        <!-- Submit button -->
        {{-- <div class="d-block w-100 text-center mt-5">
            <button type="submit" class="btn btn-primary btn-inline-block mb-4 w-25">تعديل <i class="fa-solid fa-pen-to-square"></i></button>
            <a class="btn btn-danger btn-inline-block mb-4 w-25 mr-auto ml-auto" href="{{ route('centers.index') }}">إلغاء
                <i class="fa-solid fa-xmark"></i></a>
        </div> --}}
        <br>
        <div class="d-block w-100 text-center mt-5">
        <button type="submit" class="btn  btn-inline-block mb-4 w-25 mr-auto ml-auto text-center" style="background-color: #81ecec">تعديل <i class="fa-regular fa-pen-to-square"></i></button>
        <a class="btn  btn-inline-block mb-4 w-25 mr-auto ml-auto" href="{{route('centers.index')}}" style="background-color: #fab1a0">إلغاء <i class="fa-solid fa-xmark"></i></a>
      </div>
    </form>
@endsection
