@extends('dashboardpanal.layouts.admin')

@section('title')
@endsection

@section('breadcrumb')
    <a href="{{ route('dashboard') }}" class="nav-link">الضبط العام</a>
@endsection
@section('contentheader')

@endsection
@section('contentheaderlink')
سلة المحذوفات
@endsection
@section('contentheaderactive')
المؤسسات
@endsection
@section('content')
{{-- <h1 class="text-info d-block text-center mb-4">سلة المحذوفات</h1> --}}
  <table class="table">
    <thead style="background-color: #fab1a0">
      <tr>
        <th scope="col">#</th>
        <th scope="col">اسم المركز</th>
        <th scope="col">المدينة</th>
        <th scope="col">العنوان</th>
        <th scope="col">تابعة ل</th>
        <th scope="col">ملاحظات</th>
        <th scope="col">افعال</th>
      </tr>
    </thead>
    <tbody id="itemTableBody">
      <?php $i=0?>
      @foreach ($centers as $center)
      <tr>
        <th scope="row">{{++$i}}</th>
        <td>{{Str::limit($center->name,30)}}</td>
        <td>{{$center->city->name }}</td>
        <td>{{Str::limit($center->address,30)}}</td>
        @if ($center->center_id != null)
        @if (isset($center->parent->name))
        <td>{{Str::limit($center->parent->name,30)}}</td>
        @else
        <td>-</td>
        @endif
        @else
        <td>-</td>
        @endif
        <td>{{Str::limit($center->notes,20)}}</td>
          <td>
            <form action="{{route('restore',['center'=>$center])}}" class="d-inline-block" method="GET">
                @csrf
                <button class="btn btn-outline-info" type="submit"><i class="fa-solid fa-trash-can-arrow-up"></i></button>
              </form>
          <form action="{{route('trash',['id'=>$center->id])}}" class="d-inline-block" method="GET">
            @csrf
            <button class="btn btn-outline-danger" type="submit"><i class="fa-solid fa-trash"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="d-flex">
    {!! $centers-> links()!!}
  </div>
@endsection