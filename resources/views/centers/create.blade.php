@extends('dashboardpanal.layouts.admin')

@section('title')
@endsection

@section('breadcrumb')
    <a href="{{ route('dashboard') }}" class="nav-link">الضبط العام</a>
@endsection

@section('contentheader')
@endsection

@section('contentheaderlink')
@endsection

@section('contentheaderactive')

@endsection

@section('content')
<form action="{{route('centers.index')}}" method="POST">
  @csrf
  <div class="row mb-4">
  </div>
  <div class="form-outline mb-4">
    <input type="text" id="form6Example3" class="form-control" name="name" required maxlength="80"/>
    <label class="form-label text-primary" for="form6Example3" >اسم المؤسسة</label>
  </div>
  @error('name')
  <div class="text-danger">{{$message}}</div>
  @enderror
  <!-- Text input -->
  <div class="form-outline mb-4">
    <input type="text" id="form6Example4" class="form-control" name="address" required maxlength="200"/>
    <label class="form-label text-primary" for="form6Example4">العنوان</label>
  </div>
  <!-- Message input -->
  <div class="form-outline mb-4">
    <textarea class="form-control" id="form6Example7" rows="4"></textarea>
    <label class="form-label" for="form6Example7">ملاحظات</label>
  </div>
  <div class="col-md-3 mb-3">
 
    <label for="validationDefault04"> اختر مدينة</label>
    <select class="custom-select" id="validationDefault04" required name="city_id">
      <option disabled selected>المدن</option>
      @foreach ($cities as $city)
      <option value="{{$city->id}}">{{$city->name}}</option>
      @endforeach
    </select>
  </div>
  @error('city_id')
  <div class="text-danger">{{$message}}</div>
  @enderror
  <div class="col-md-3 mb-3">
    <label for="validationDefault04">تابعة ل</label>
    <select class="custom-select" id="validationDefault04" required name="center_id">
      <option disabled selected>مؤسسة</option>
      @foreach ($centers as $center)
      <option value="{{$center->id}}">{{$center->name}}</option>
      @endforeach
    </select>
  </div>
  <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
  <br>
  <div class="d-block w-100 text-center mt-5">
  <button type="submit" class="btn  btn-inline-block mb-4 w-25 mr-auto ml-auto text-center" style="background-color: #81ecec">إنشاء <i class="fa-solid fa-plus"></i></button>
  <a class="btn  btn-inline-block mb-4 w-25 mr-auto ml-auto" href="{{route('centers.index')}}" style="background-color: #fab1a0">إلغاء <i class="fa-solid fa-xmark"></i></a>
</div>
</form>
@endsection