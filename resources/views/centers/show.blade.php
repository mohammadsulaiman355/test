@extends('dashboardpanal.layouts.admin')

@section('title')
@endsection

@section('breadcrumb')
    <a href="{{ route('dashboard') }}" class="nav-link">الضبط العام</a>
@endsection

@section('contentheader')
@endsection

@section('contentheaderlink')
عرض
@endsection
@section('contentheaderactive')
المؤسسات
@endsection

@section('content')
<div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">اسم المؤسسة</a>
      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">العنوان</a>
      <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">المدينة</a>
      <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">تابعة ل </a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
      <h1 class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">{{$center->name}}</h1>
      <h1 class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">{{$center->address}}</h1>
      <h1 class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">{{$center->city->name}}</h1>
      <h1 class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
        @if (isset($center->parent->name))
        {{$center->parent->name}}
        @else
        مركز رئيسي
        @endif
      </h1>
    </div>
  </div>
</div>
@endsection
